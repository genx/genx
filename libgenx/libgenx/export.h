#ifndef LIBGENX_EXPORT_H
#define LIBGENX_EXPORT_H

#if defined(LIBGENX_STATIC)         /* Using static. */
#  define LIBGENX_SYMEXPORT
#elif defined(LIBGENX_STATIC_BUILD) /* Building static. */
#  define LIBGENX_SYMEXPORT
#elif defined(LIBGENX_SHARED)       /* Using shared. */
#  ifdef _WIN32
#    define LIBGENX_SYMEXPORT __declspec(dllimport)
#  else
#    define LIBGENX_SYMEXPORT
#  endif
#elif defined(LIBGENX_SHARED_BUILD) /* Building shared. */
#  ifdef _WIN32
#    define LIBGENX_SYMEXPORT __declspec(dllexport)
#  else
#    define LIBGENX_SYMEXPORT
#  endif
#else
/* If none of the above macros are defined, then we assume we are being used
// by some third-party build system that cannot/doesn't signal the library
// type. Note that this fallback works for both static and shared libraries
// provided the library only exports functions (in other words, no global
// exported data) and for the shared case the result will be sub-optimal
// compared to having dllimport. If, however, your library does export data,
// then you will probably want to replace the fallback with the (commented
// out) error since it won't work for the shared case.
*/
#  define LIBGENX_SYMEXPORT
#endif

#endif /* LIBGENX_EXPORT_H */
