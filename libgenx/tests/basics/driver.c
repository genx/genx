#include <stdio.h>
#include <string.h>

#include <libgenx/version.h>
#include <libgenx/genx.h>

static genxStatus
genxSend(void * d, constUtf8 s)
{
  size_t n = strlen((const char*)s);
  return fwrite(s, 1, n, (FILE*)d) == n ? GENX_SUCCESS : GENX_IO_ERROR;
}

static genxStatus
genxSendBounded(void * d, constUtf8 start, constUtf8 end)
{
  size_t n = end - start;
  return fwrite(start, 1, n, (FILE*)d) == n ? GENX_SUCCESS : GENX_IO_ERROR;
}

static genxStatus
genxFlush(void * d)
{
  return fflush((FILE*)d) == 0 ? GENX_SUCCESS : GENX_IO_ERROR;
}

static const genxSender stdioSender = {&genxSend, &genxSendBounded, genxFlush};

int main()
{
  genxWriter w = genxNew(NULL, NULL, stdout);
  genxStartDocSender(w, &stdioSender);
  genxStartElementLiteral(w, (utf8)"http://example.org/1", (utf8)"greeting");
  genxAddAttributeLiteral(w, (utf8)"http://example.com/2", (utf8)"type", (utf8)"well-formed");
  genxAddText(w, (utf8)"Hello world!");
  genxEndElement(w);
  genxEndDocument(w);
  genxDispose(w);

  return 0;
}
