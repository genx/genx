# libgenx - XML serializer C library

A portable, dependency-free, MIT-licensed XML push-serializer library for C.

```
#include <libgenx/genx.h>

int main()
{
  genxWriter w = genxNew(NULL, NULL, stdout);
  genxStartDocSender(w, &stdioSender);
  genxStartElementLiteral(w, (utf8)"http://example.org/1",
                             (utf8)"greeting");
  genxAddAttributeLiteral(w, (utf8)"http://example.com/2",
                             (utf8)"type",
			     (utf8)"well-formed");
  genxAddText(w, (utf8)"Hello world!");
  genxEndElement(w);
  genxEndDocument(w);
  genxDispose(w);
}
```

This is a cleaned up, fixed, and enhanced version of [Tim Bray's
Genx](https://www.tbray.org/ongoing/genx/docs/Guide.html). Note that support
for `genxStartDocFile()` (which depends on `<stdio.h>`) has been removed.
See the `basics` test for an example of how to achieve the same yourself.
